package bookshop;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.sql.*;

@SuppressWarnings("serial")
public class Store extends HttpServlet {
    private Statement statement;
    PrintWriter out;
    
    public Store() {
    	super();
    }
    
    protected void doPost (HttpServletRequest request, HttpServletResponse response) {
        //response.setContentType("text/html");
        try {
			out = response.getWriter();
		} catch (IOException e1) {
			System.out.println("Could not write on HTML file.");
		}
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Bookshop</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1>Author's Book(s):</h1>");
        
    	try {
        	Class.forName("com.mysql.jdbc.Driver").newInstance();
        	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookshop","root", "");
        	System.out.print("Database is connected!");
            statement = conn.createStatement();
            statement = conn.createStatement();
        }
        catch (Exception e) {
           	System.out.println("Could not connect to Database. Please try again.");
        	out.println("Could not connect to Database. Please try again.");
        	return;
        }

		String[] authors = request.getParameterValues("author");
		String query = "";

		try {
			
			for(int j = 0; j < authors.length; j++) {
        		query = "SELECT author, title, price FROM books WHERE author = '" + authors[j] + "';";
                ResultSet rs = statement.executeQuery(query);
                while (rs.next()) {
                    String author = rs.getString("author");
                    String title = rs.getString("title");
                    float price = rs.getFloat("price");
                    out.println(author + ", " + title + ", $" + price + ".");
                    out.println("<br><br>");
                }
                
        	}

        }
        catch (Exception f) {
        	out.println("No authors were selected.");
        }

		
        out.println("</body>");
        out.println("</html>");
        out.close();
    }
}