

import java.io.*;
import java.net.*;

/**
* Read_and_print.java - A simple class used to read a web page and print the HTML source code of it.
* @author A6 - Giulia Baldini, Emanuela Calabi, Mikel Grabocka
* Done for the Internet and Mobile Services (IMS) course of the Free University of Bolzano-Bozen.
*/

public class Read_and_print {
	
	public static void main (String[] args) throws Exception{
		URL url = new URL("https://www.unibz.it/en");
        URLConnection urlconn = url.openConnection();
        InputStream input = urlconn.getInputStream();
        String encoding = urlconn.getContentEncoding();
       
        if (encoding == null) {
        	encoding = "UTF-8";
        }
       
        ByteArrayOutputStream outsream = new ByteArrayOutputStream();
        byte[] buffer = new byte[8192];
        int length = input.read(buffer);
        while (length != -1) {
        	outsream.write(buffer, 0, length);
        	length = input.read(buffer);
        }
        String content = new String(outsream.toByteArray(), encoding);
        System.out.println(content);
	}
	
}
