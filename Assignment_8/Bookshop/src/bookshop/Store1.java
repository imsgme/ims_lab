package bookshop;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.*;
import java.sql.*;


public class Store1 extends HttpServlet {
    private Statement statement;
    PrintWriter out;
    
    public Store1() {
    	super();
    }
    
    protected void doGet (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        
    	try {
        	Class.forName("com.mysql.jdbc.Driver").newInstance();
        	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookshop","root", "");
        	System.out.print("Database is connected!");
            statement = conn.createStatement();
            statement = conn.createStatement();
        }
        catch (Exception e) {
           	System.out.println("Could not connect to Database. Please try again.");
        	out.println("Could not connect to Database. Please try again.");
        	return;
        }

		String query = "";
		ResultSet rs = null ;
		try {
			
        		query = "SELECT distinct author FROM books";
                rs = statement.executeQuery(query);

        }
        catch (Exception f) {
        	out.println("No authors were selected.");
        }

		request.setAttribute("message", rs);
    }
    
}