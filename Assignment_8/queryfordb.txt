
CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `qty` int(11) DEFAULT NULL
) 

INSERT INTO `books` (`id`, `title`, `author`, `price`, `qty`) VALUES
(1001, 'Matrix Decomposition for Recommender Systems', 'Panagiotis Symeonidis', 20, 10), 
(1002, 'Data Mining', 'Charu C. Aggarwal', 30, 15), 
(1003, 'Java: The Complete Reference', 'Herbert Schildt', 40, 20), 
(1004, 'Data bases', 'Ramakrishnan and Gehrke', 50, 25), 
(1005, 'Recommenders for Location-based Social Networks', 'Panagiotis Symeonidis', 50, 30);
