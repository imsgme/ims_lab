$(document).ready(function() {
	$("[tooltip]")
	
	.mouseover(function(e) {
		
		var text = $(this).attr("tooltip");
		var wrapper = $(document.createElement("div"));
		$(this)[0].tooltip = wrapper;
		wrapper.text(text);
		
		wrapper.css("position", "absolute");
		wrapper.css("left", e.pageX + 5 + "px");
		wrapper.css("top", e.pageY + 5 + "px");
		wrapper.css("display", "block");
		
		wrapper.css("padding", "5px");
		wrapper.css("font-size", "12px");
		
		wrapper.css("color", "#525252");
		wrapper.css("background", "#FFFFFF");
		wrapper.css("border", "1px solid #525252");
		wrapper.css("box-shadow", "5px 5px rgba(52, 52, 52, 0.4)");
		
		wrapper.appendTo(document.body);
	})
	
	.mouseout(function() {
		$(this)[0].tooltip.remove();
	});
});


