<html>

<head>
<title>Functions</title>
</head>

<body>

<?php

function ctable($number1,$number2){
  $total = $number1 + $number2;
  echo '<table width="200" border="1">';
  echo '<tr>';
  echo '<td>' . $number1 . '</td>';
  echo '<td>' . $number2 . '</td>';
  echo '<td>' . $total . '</td>';
  echo '</tr>';
  echo '</table> <br><br>';
}

function add_1($number1,$number2){
  $total = $number1 + $number2;
  $total2 = $number1 + 10;
  return $total;
}

function add_2($number1,$number2){
  global $gtotal;
  $gtotal = $number1 + $number2;
}

function add_range($number1,$number2){

  if ($number1 > $number2){
    $max = $number1;
    $min = $number2;
  }else{
    $min = $number1;
    $max = $number2;
  }

  $total = 0;

  for (; $min <= $max; $min++){ 
    $total = $total + $min;
  }

  return $total;
}

ctable(4,2);

echo '2+3='. add_1(2,3) . '<br><br>';

add_2(5,5);
echo '5+5=' . $gtotal . '<br><br>';

echo 'range (4-8) = 4+5+6+7+8 = ' . add_range(4,8) . '<br><br>';

echo 'range (6-2) = 2+3+4+5+6 = ' . add_range(6,2) . '<br><br>';

echo 'range (2-2) = 2+2 = ' . add_range(2,2) . '<br><br>';

?>

</body>
</html>