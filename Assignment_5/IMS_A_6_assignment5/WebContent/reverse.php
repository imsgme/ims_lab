<html>

<head>
<title>Recursion</title>
<style>
#content input{
    width:100%;
    box-sizing:border-box;
    -moz-box-sizing:border-box;
}
</style>
</head>

<body>

<div id='content' style='width:100%;'>
  <form method="post" action="reverse.php">
  	<br>Please enter some text. Enter to stop.<br>
  	<input type="text" name="input" id="textbox"><br>
  </form>
</div>

<?php

function reverse_r($str){
  if (strlen($str)>0){
    reverse_r(substr($str, 1));
  }
  echo substr($str, 0, 1); 
  return;
}

function reverse_i($str){
  for ($i=1; $i<=strlen($str); $i++){
    echo substr($str, -$i, 1);
  }
  return;
}

if (isset($_POST['input'])){
  $text = $_POST['input'];
  echo "<b>The original sentence was:</b>", "<br>";
  echo $text, "<br><br>";
  echo "<b>The reversed sentence is:</b>", "<br>";
  $words = explode(" ", $text);
  for ($i=0; $i < count($words); $i++) { 
  	if ($i%2==0){
  	  echo $words[$i], " ";
  	} else {
  	  echo reverse_r($words[$i]), " ";
  	}
  }
}

?>
</body>
</html>


