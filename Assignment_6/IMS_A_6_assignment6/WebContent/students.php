<html>

<head>
<title>Students</title>
<script>
  var toggle = function() {
  var mydiv = document.getElementById('container');
  if (mydiv.style.display === 'block' || mydiv.style.display === '')
    mydiv.style.display = 'none';
  else
    mydiv.style.display = 'block'
  }
</script>
</head>

<body>
<div id='table'>
<?php
//Connection to database
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "school";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
} 

//Function to create a row for each element
function create_table($string, $value){
	echo "<tr>";
  echo "<td width='50%' bgcolor='#EAE9FF' align='right'>";
  echo $string;
  echo "</td>";
  echo "<td width='50%' bgcolor='#EAE9FF'>";
  echo $value;
  echo "</td>";
  echo "</tr>";
}

//If there is a post event to add a new student
if(isset($_POST['new'])){
  $sql = "SELECT max(id) as max from students;";
  $result = $conn->query($sql);
  $row = $result->fetch_assoc();
  $id = $row['max'] + 1;

  $lastname = $_POST['lastname'];
  $firstname = $_POST['firstname'];
  $address = $_POST['address'];
  $tel = $_POST['tel'];

  $sql = "INSERT INTO students VALUES (" . $id . ", '" . $lastname . "', '" . $firstname . "', '" . $address . "', '" . $tel . "');";

  $result = $conn->query($sql);

  header('Location: '.$_SERVER['PHP_SELF']);
}

//If there is a new post event to modify/insert the grades
if(isset($_POST['grades'])){

  $name = $_POST['student_name'];
  $course = $_POST['course_title'];
  $grade = $_POST['grade'];
  global $course_id;
  global $student_id;

  $sql = "SELECT students.id as sid, lessons.id as lid FROM students, lessons where students.surname = '" . $name . "' and lessons.name = '" . $course . "';";

  $result = $conn->query($sql);

  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      $course_id = $row['lid'];
      $student_id = $row['sid'];
    }
  }

  $sql = "SELECT * FROM grades, lessons, students where lessons.id = grades.lesson_id AND students.id = grades.student_id and name = '" . $course . "' and surname = '" . $name . "';";
  $result = $conn->query($sql);
  $row = $result->fetch_assoc();

  $send = "";

  if ($result->num_rows > 0){
    $sql = "UPDATE grades set grade = " . $grade . " where student_id = (select id from students where surname = '" . $name . "') and lesson_id = (select id from lessons where name = '" . $course . "');";
    $send = $send . "u;";
  }else {
    $sql = "INSERT INTO grades values ( " . $student_id . ", " . $course_id . ", " . $grade . ");";
    $send = $send . "i;";
  }

  $result = $conn->query($sql);

  $send = $send . $name . ";" . $course;
  header('Location: updated_grades.php?send='.$send);
}

$sql = "SELECT id, surname, first_name, address, telephone FROM students";
$result = $conn->query($sql);

//Use create table to create the rows
if ($result->num_rows > 0) {
  while($row = $result->fetch_assoc()) {
  	echo "<table align='center'>";
  	create_table("Student id", $row['id']);
  	create_table("Student Surname", $row['surname']);
  	create_table("Student Name", $row['first_name']);
  	create_table("Address", $row['address']);
  	create_table("Telephone", $row['telephone']);
  	echo "<br>";
		echo "</table>";
  }
} else {
    echo "0 results";
}

?>
</div>
<br><br>
<div align="center">
	<button id='new_button' onclick="toggle();">Open new form</button><br>
</div>
<div id='container' align='center' style="display:none">
	<form id='new_form' action='students.php' method='post'>
		<br>
		<label for="lastname">Last name:</label>
       	<input tooltip="Enter last name" id="lastname" class="form-control input-group-lg" type="text" name="lastname"placeholder="Last name" required/>
       	<br>
       	<label for="firstname">First name:</label>
       	<input tooltip="Enter first name" id="firstname" class="form-control input-group-lg" type="text" name="firstname" placeholder="First name" required/>
       	<br>
       	<label for="address">Address:</label>
       	<input tooltip="Enter address" id="address" class="form-control input-group-lg" type="text" name="address" placeholder="Street and number" required/>
       	<br>
       	<label for="firstname">Telephone</label>
       	<input tooltip="Enter telephone number" id="tel" class="form-control input-group-lg" type="text" name="tel" placeholder="Telephone number" required/><br><br>
       	<button type="submit" name='new' id='sub'>Submit data</button>
	</form>
</div>
<br><br>
<div align="center">

<form id='grades' action='students.php' method='post'>
  <label for="student_name">Student details: </label>
  <select name='student_name'>
  <?php
  $sql = "SELECT surname FROM students ORDER BY surname ASC";
  $result = $conn->query($sql);

  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      echo "<option value='". $row['surname']."'>".$row['surname']."</option>";      
    }
  }
  ?>
  </select>
  <label for="course_title">Course title:</label>
  <select name="course_title">
  <?php
  $sql = "SELECT name FROM lessons ORDER BY name ASC";
  $result = $conn->query($sql);
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      echo "<option value='". $row['name']."'>". $row['name']."</option>";   
    }
  }
  ?>
  </select>
  <label for="insert_grade">Grades:</label>
  <select name="grade">
  <?php
  for($i=1; $i <= 31; $i= $i+1){
     echo "<option value='" . $i . "'>" . $i . "</option>";
  }
  ?>
  </select>
  <br><br>

  <button type="submit" id="update_grade" name="grades">Update grades</button>
</form>

<?php
$conn->close();
?>

</div>

</body>
</html>